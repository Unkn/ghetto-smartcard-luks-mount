#!/bin/sh -x

dest='/enc/home'
if [ ! -z "$1" ]; then
    dest="$1"
fi

mkdir_exit()
{
    if [ ! -d "$1" ]; then
        echo Making directory $1
        if ! mkdir -p $1; then
            >&2 Failed to make directory $1
            exit 1
        fi
    fi
}

here="$(dirname $(realpath $0))"

. "$here/mount.cfg"

key_enc="$here/key.enc"
rdir="$here/ramfs"
key_dec="$rdir/key.dec"
dev=$(ls /dev/disk/by-partuuid -lt | grep $UUID | sed 's/ /\n/g' | tail -n 1 | cut -d '/' -f 3)
mapper=/dev/mapper/$MAPPING_NAME

mkdir_exit "$dest"
mkdir_exit "$rdir"

mount -t ramfs -o size=1M ramfs "$rdir"
chmod -R u=rwx,g=,o= "$rdir"

if ! pkcs15-crypt --key $SC_KEY_ID --decipher --pkcs1 --raw --input "$key_enc" -o "$key_dec"; then
    >&2 echo Failed to decrypt filesystem key
    umount "$rdir"
    exit 1
fi

cryptsetup luksOpen --key-file=$key_dec /dev/$dev $MAPPING_NAME

shred "$key_dec"
rm "$key_dec"
umount "$rdir"

mount $mapper $dest
