#!/bin/sh -x

. "$(dirname $(realpath $0))/mount.cfg"

umount $(mount | grep $MAPPING_NAME | cut -d ' ' -f 3)
cryptsetup luksClose $MAPPING_NAME
